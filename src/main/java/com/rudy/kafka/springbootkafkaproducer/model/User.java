package com.rudy.kafka.springbootkafkaproducer.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@Setter
@Getter
public class User {
	
	private String name;
	private String dept;
	private Long salary;
	private String gender;
	
	public User(String name, String dept, Long salary, String gender) {
		this.name = name;
		this.dept = dept;
		this.salary = salary;
		this.gender = gender;
	}
}
